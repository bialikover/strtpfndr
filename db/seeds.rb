# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

puts 'SETTING UP DEFAULT USER LOGIN'
user = User.create! :name => 'Admin', :email => 'foo@foobar.com', :password => 'foobar123*-', :password_confirmation => 'foobar123*-'
puts 'New user created: ' << user.name
user2 = User.create! :name => 'Second User', :email => 'bialikoer@gmail.com', :password => 'cristovive123*-', :password_confirmation => 'cristovive123*-'
puts 'New user created: ' << user2.name